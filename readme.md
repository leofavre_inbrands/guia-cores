Guia Cores
==========

Recebemos a informação do SAC deque os clientes da VR estão com dificuldade em saber quais cores estão selecionadas, especialmente quando são próximas.

Visualmente, seria melhor se os seletores estivessem separados entre si, pois estão todos grudados.

Em termos de funcionalidade, temos um padrão, no site da Richards atual, em que o nome da cor é mostrado no momento da seleção, mas mesmo esse padrão poderia ser melhorado da seguinte forma:

* No mouseover, um "tooltip" que mostra o nome da cor.

![Seletor de cores atualizado](http://www.richards.com.br/cores/rch-cores-01.png?abc)

* Como não podemos contar com o mouseover em tablets, o nome da cor selecionada também aparece abaixo do seletor, quando a seleção ocorre, exatamente como no site da Richards.

![Seletor de cores atualizado](http://www.richards.com.br/cores/rch-cores-02.png)